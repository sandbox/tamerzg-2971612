<?php

namespace Drupal\cointraffic\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;


/**
 * Class CoinTrafficMainSettings.
 */
class CoinTrafficMainSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cointraffic_main_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cointraffic.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('cointraffic.settings');
    /*
        $form['help'] = [
          '#type' => 'details',
          '#open' => FALSE,
          '#title' => $this->t('Help and instructions'),
        ];

        $form['help']['help'] = ['#markup' => adsense_help_text()];
    */
    $form['cointraffic_wkey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site CoinTraffic wkey'),
      '#required' => TRUE,
      '#default_value' => $config->get('cointraffic_wkey'),
      '#description' => $this->t('This is the CoinTraffic wkey for the site owner. Get this from http://cointraffic.io'),
    ];
    /*
    $form['adsense_unblock_ads'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display anti ad-block request?'),
      '#default_value' => $config->get('adsense_unblock_ads'),
      '#description' => $this->t("EXPERIMENTAL! Enabling this feature will add a mechanism that tries to detect when adblocker software is in use, displaying a polite request to the user to enable ads on this site. [@moreinfo]",
        ['@moreinfo' => Link::fromTextAndUrl($this->t('More information'), Url::fromUri('http://easylist.adblockplus.org/blog/2013/05/10/anti-adblock-guide-for-site-admins'))->toString()]
      ),
    ];

    $form['adsense_test_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable test mode?'),
      '#default_value' => $config->get('adsense_test_mode'),
      '#description' => $this->t('This enables you to test the AdSense module settings. This can be useful in some situations: for example, testing whether revenue sharing is working properly or not without having to display real ads on your site. It is best to test this after you log out.'),
    ];

    $form['adsense_disable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable Google AdSense ads?'),
      '#default_value' => $config->get('adsense_disable'),
      '#description' => $this->t('This disables all display of Google AdSense ads from your web site. This is useful in certain situations, such as site upgrades, or if you make a copy of the site for development and test purposes.'),
    ];

    $form['adsense_placeholder'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Placeholder when ads are disabled?'),
      '#default_value' => $config->get('adsense_placeholder'),
      '#description' => $this->t('This causes an empty box to be displayed in place of the ads when they are disabled.'),
    ];

    $form['adsense_placeholder_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Placeholder text to display'),
      '#default_value' => $config->get('adsense_placeholder_text'),
      '#rows' => 3,
      '#description' => $this->t('Enter any text to display as a placeholder when ads are disabled.'),
    ];
*/
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('cointraffic.settings');
    $form_state->cleanValues();

    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, Html::escape($value));
    }
    $config->save();
  }
}
